# Rx coil housing

CAD drawing of a Rx coil housing for human head imaging. More information on the coil can be found in Gilbert KM et al. Proceedings of the 23rd ISMRM 2015:0623.